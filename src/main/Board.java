package main;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;

public class Board {

    public char[][] table;
    public HashSet<Point> singletons = new HashSet<Point>();
    public LinkedList<Line> winningLines = new LinkedList<Line>();
    char emptyCell;
    char xCell;
    char zeroCell;
    private HashSet<Line> finalLines = new HashSet<>();

    public int dimension() { return table.length; }

    private Board() {}

    public Board(int dimension, char emptyCell, char xCell, char zeroCell) {
        this.emptyCell = emptyCell;
        this.zeroCell = zeroCell;
        this.xCell = xCell;
        table = new char[dimension][dimension];
        for(int i = 0; i < dimension; i++)
            for(int j = 0; j < dimension; j++)
                table[i][j] = emptyCell;
    }

    public char getCell(Point p) {
        return getCell(p.row, p.column);
    }

    public char getCell(int row, int column) {
        return table[row][column];
    }

    public boolean isEmptyCell(int row, int column) {
        return table[row][column] == emptyCell;
    }

    public boolean isEmptyCell(Point p) {
        return p != null && isEmptyCell(p.row, p.column);
    }

    public void set(int row, int column, char c) {
        table[row][column] = c;
        Point current = new Point(row, column);
        LinkedList<Point> neighbors = getNeighbors(current, 1);
        if (neighbors.isEmpty()) {
            singletons.add(current);
            return;
        }
        winningLines.stream()
                .filter(line -> line.isNext(current, this))
                .forEach(line -> {
                    line.add(current);
                    singletons.remove(current);
                });
        neighbors.forEach(n -> {
            Line newLine = new Line(current, n);
            if (!winningLines.contains(newLine)) {
                winningLines.add(newLine);
                singletons.remove(current);
                singletons.remove(n);
            }
        });
        concatOverlappingLines();
        deleteFinalLines();
        winningLines.sort(Comparator.comparingInt(Line::length).reversed());
    }

    public void set(Point p, Character c) {
        set(p.row, p.column, c);
    }

    private void deleteFinalLines() {
        winningLines.removeIf(line -> {
            char startCell = getCell(line.getStart());
           LinkedList<Point> edging = getEdgingPoints(line);
           edging.removeIf(p -> getCell(p) != startCell && !isEmptyCell(p));
           if(edging.size() == 0)
               finalLines.add(line);
           return edging.size() == 0 || finalLines.stream().anyMatch(l -> l.contains(line));
        });
    }

    private void concatOverlappingLines() {
        for(int i = 0; i < winningLines.size(); i++) {
            Line Li = winningLines.get(i);
            for(int j = 0; j < winningLines.size(); j++) {
                if (j == i)
                    continue;
                Line Lj = winningLines.get(j);
                if (Li.contains(Lj)) {
                    winningLines.remove(Lj);
                    j--;
                    continue;
                }
                if (Li.isContinuedBy(Lj)) {
                    Li.concat(Lj);
                    winningLines.remove(Lj);
                    j--;
                }
            }
        }
    }

    public boolean isOnBoard(int row, int column) {
        return row >= 0 && row < dimension() && column >= 0 && column < dimension();
    }

    public boolean isOnBoard(Point p) {
        return isOnBoard(p.row, p.column);
    }

    public LinkedList<Point> getEdgingPoints(Line line) {
        int[] direction = line.direction();
        LinkedList<Point> answer = new LinkedList<>();
        answer.add(new Point(line.getStart().row - direction[0], line.getStart().column - direction[1]));
        answer.add(new Point(line.getEnd().row + direction[0], line.getEnd().column + direction[1]));
        answer.removeIf(p -> !isOnBoard(p));
        return answer;
    }

    public LinkedList<Point> getEdgingPoints(Point start, Point end) {
        return getEdgingPoints(new Line(start, end));
    }

    public Point getEmptyCellNearWith(Point p) {
        ArrayList<Point> answer = new ArrayList<Point>();
        answer.add(new Point(p.row - 1, p.column));
        answer.add(new Point(p.row, p.column - 1));
        answer.add(new Point(p.row - 1, p.column - 1));
        answer.add(new Point(p.row + 1, p.column));
        answer.add(new Point(p.row, p.column + 1));
        answer.add(new Point(p.row + 1, p.column + 1));
        answer.add(new Point(p.row - 1, p.column + 1));
        answer.add(new Point(p.row + 1, p.column - 1));
        for (Point point : answer)
            if (isOnBoard(point))
                if(isEmptyCell(point))
                    return point;
        return null;
    }

    public LinkedList<Point> getNeighbors(int row, int column, int range) {
        LinkedList<Point> answer = new LinkedList<>();
        answer.add(new Point(row - range, column));
        answer.add(new Point(row, column - range));
        answer.add(new Point(row - range, column - range));
        answer.add(new Point(row + range, column));
        answer.add(new Point(row, column + range));
        answer.add(new Point(row + range, column + range));
        answer.add(new Point(row - range, column + range));
        answer.add(new Point(row + range, column - range));
        answer.removeIf(p -> !isOnBoard(p) || getCell(p) != getCell(row, column));
        return answer;
    }

    public LinkedList<Point> getNeighbors(Point p, int range) {
        return getNeighbors(p.row, p.column, range);
    }

    public void show() {
        int i = 0;
        System.out.print(" ");
        for (;i < dimension(); i++)
            System.out.printf(" %d", i);
        System.out.println();
        for (i = 0; i < dimension(); i++) {
            System.out.printf("%d %s", i, table[i][0]);
//            System.out.println(String.join("|", String.valueOf(table[i])));
            for (int j = 1; j < dimension(); j++) {
                System.out.print(isEmptyCell(i, j) ? "| " : "|" + table[i][j]);
            }
            System.out.println();
            if (i != dimension() - 1) {
                System.out.print("  ");
                for (int j = 0; j < dimension() ; j++)
                    System.out.print("- ");
                System.out.println();
            }
        }
    }

}
