package main;

import java.util.LinkedList;

public class AI implements Player {

    private class Pair<T, E> {
        T first;
        E second;
    }

    private Board board;
    private char character;
    private int winningLength;

    private AI() {}

    public AI(Board board, char character, int winningLength) {
        this.board = board;
        this.character = character;
        this.winningLength = winningLength;
    }

    @Override
    public void turn() {
        board.set(nextStep(), character);
    }

    @Override
    public Point nextStep() {
        if (!board.winningLines.isEmpty()) {
            Line first = board.winningLines.getFirst();
            Pair<Line, Line> longestLinesPair = new Pair<>();
            Pair<Point, Line> longestLineAndPointPair = new Pair<>();
            for(Line Li : board.winningLines) {
                for(Line Lj : board.winningLines) {
                    if(!isLinesWithGap(Li, Lj, 2) || board.getCell(Li.getEnd()) != board.getCell(Lj.getEnd())
                    || Lj == Li)
                        continue;
                    if(longestLinesPair.first == null || Lj.length() + Li.length() >
                            longestLinesPair.first.length() + longestLinesPair.second.length()) {
                        longestLinesPair.first = Lj;
                        longestLinesPair.second = Li;
                    }
                }
                if(longestLineAndPointPair.first == null || Li.length() + 2 > longestLineAndPointPair.second.length()) {
                    int [] dir = Li.direction();
                    Point fst = new Point(Li.getStart().row - dir[0] * 2, Li.getStart().column - dir[1]*2);
                    Point snd = new Point(Li.getEnd().row + dir[0] * 2, Li.getEnd().column + dir[1] * 2);
                    if(board.isOnBoard(fst) && board.getCell(fst) == board.getCell(Li.getEnd())) {
                        longestLineAndPointPair.first = fst;
                        longestLineAndPointPair.second = Li;
                    } else if(board.isOnBoard(snd) && board.getCell(snd) == board.getCell(Li.getEnd())) {
                        longestLineAndPointPair.first = snd;
                        longestLineAndPointPair.second = Li;
                    }
                }
            }
            if(longestLinesPair.first != null) {
                Point gap = closeGap(longestLinesPair.first, longestLinesPair.second);
                if(longestLinesPair.first.length() + longestLinesPair.second.length() + 1 >= first.length()
                        && board.isEmptyCell(gap))
                    return gap;

            }
            if(longestLineAndPointPair.second != null) {
                Point gap = closeGap(longestLineAndPointPair.second, longestLineAndPointPair.first);
                if(longestLineAndPointPair.second.length() + 2 >= first.length()
                        && board.isEmptyCell(gap))
                    return gap;
            }
            LinkedList<Point> edging = board.getEdgingPoints(first);
            edging.removeIf(p -> !board.isEmptyCell(p));
            return edging.peek();
        }
        if(winningLength == 3)
            for(Point p : board.singletons) {
                if(board.getCell(p) != character)
                    continue;
                Point neighbor = board.getNeighbors(p, 2).peek();
                if(neighbor != null)
                    return neighbor;
            }
        for(Point p : board.singletons) {
            if (board.getCell(p) == character)
                continue;
            Point empty = board.getEmptyCellNearWith(p);
            if (empty != null)
                return empty;
        }
        return firstStep();
    }

    private boolean isLinesWithGap(Line line1, Line line2, int range) {
        return line1.isOnSameStraight(line2, board) && line1.getNearestPoints(line2).length() <= range;
    }

    private Point firstStep() {
        int half = board.dimension() / 2;
        if (!board.isEmptyCell(half, half))
            half ++;
        return new Point(half, half);
    }

    private Point closeGap(Line line, Point p) {
        Point _p = line.getNearestPointTo(p);
        return stepBetween(p, _p);
    }

    private Point closeGap(Line line1, Line line2) {
        Line l = line1.getNearestPoints(line2);
        return new Point(Math.abs(l.getY()), Math.abs(l.getX()));
    }

    private Point stepBetween(Point p1, Point p2) {
        Point right = p1.column <= p2.column ? p1 : p2;
        Point left = p1.column > p2.column ? p1 : p2;
        Line line = new Line(right, left);
        int[] dir = line.direction();
        return new Point(right.row + dir[0], right.column + dir[1]);
    }

    public Point stepBetween(int row1, int column1, int row2, int column2) {
        return stepBetween(new Point(row1, column1), new Point(row2, column2));
    }

    @Override
    public Board getBoard() {
        return board;
    }

    @Override
    public void setBoard(Board board) {
        this.board = board;
    }

    @Override
    public char getCharacter() {
        return character;
    }

    @Override
    public void setCharacter(char character) {
        this.character = character;
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }

}
