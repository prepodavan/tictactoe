package main;

public interface Player {

    public Point nextStep();
    public void turn();

    public char getCharacter();
    public void setCharacter(char c);

    public Board getBoard();
    public void setBoard(Board b);

}
