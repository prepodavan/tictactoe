package main;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.Optional;

public class Line {

    private Point start;
    private Point end;
    private int x;
    private int y;
    private Point coordinates;

    private Line() {}

    public Line(Point start, Point end) {
        this.start = start;
        this.end = end;
        y = end.row - start.row;
        x = end.column - start.column;
        coordinates = new Point(x, y);
    }

    public Line(int startRow, int startColumn, int endRow, int endColumn) {
        this.start = new Point(startRow, startColumn);
        this.end = new Point(endRow, endColumn);
        y = endRow - startRow;
        x = endColumn - startColumn;
        coordinates = new Point(x, y);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Point getEnd() {
        return end;
    }

    public Point getStart() {
        return start;
    }

    public Point getCoordinates() {
        return coordinates;
    }

    public void setEnd(Point end) {
        this.end = end;
        updateCoordinates();
    }

    public void setStart(Point start) {
        this.start = start;
        updateCoordinates();
    }

    public void setEnd(int row, int column) {
        end.column = column;
        end.row = row;
        updateCoordinates();
    }

    public void setStart(int row, int column) {
        start.column = column;
        start.row = row;
        updateCoordinates();
    }

    private void updateCoordinates() {
        x = end.column - start.column;
        y = end.row - start.row;
        coordinates.row = x;
        coordinates.column = y;
    }

    int length() {
        return 1 + Math.abs(getY() == 0 ? getX() : getY());
    }

    public boolean isContinuedBy(Line line) {
        return collinear(line) && (contains(line.start) || contains(line.end));
    }

    public void concat(Line line) {
        Point myCommon;
        Point outsideCommon;
        if (line.end.equals(end)) {
            myCommon = end;
            outsideCommon = line.end;
        } else if (line.end.equals(start)) {
            myCommon = start;
            outsideCommon = end;
        } else if (line.start.equals(end)) {
            myCommon = end;
            outsideCommon = start;
        } else {
            myCommon = start;
            outsideCommon = line.start;
        }

        setStart(start.equals(myCommon) ? end : start);
        setEnd(line.start.equals(outsideCommon) ? line.end : line.start);
    }

    public void add(int row, int column) {
        int[] distance = getDistancesFromEnds(row, column);
        if (distance[0] < distance[1])
            setStart(row, column);
        else
            setEnd(row, column);
    }

    public void add(Point p) {
        add(p.row, p.column);
    }

    public int[] getDistancesFromEnds(int row, int column) {
        int startDistance = Math.max(Math.abs(row - start.row), Math.abs(column - start.column));
        int endDistance = Math.max(Math.abs(row - end.row), Math.abs(column - end.column));
        return new int[]{startDistance, endDistance};
    }

    public int[] getDistancesFromEnds(Point p) {
        return getDistancesFromEnds(p.row, p.column);
    }

    public boolean isNext(Point p, Board b) {
        return isNext(p.row, p.column, b);
    }

    public boolean isNext(int row, int column, Board board) {
        if (board.getCell(row, column) != board.getCell(start))
            return false;
        if (contains(row, column))
            return false;
        int[] distance = getDistancesFromEnds(row, column);
        if (distance[0] != 1 && distance[1] != 1)
            return false;
        int[] dir = direction();
        Point nearest = getNearestPointTo(row, column);
        int[] currentPointSign = Line.getSign(nearest.row, nearest.column, row, column);
        return dir[0] == currentPointSign[0] && dir[1] == currentPointSign[1];
    }

    public boolean contains(int row, int column) {
        boolean collinear =
                (column - start.column) * getY() - (row - start.row) * getX() == 0.0;
        boolean betweenRow =
                (start.row <= row && row <= end.row) || (end.row <= row && row <= start.row);
        boolean betweenColumn =
                (start.column <= column && column <= end.column) || (end.column <= column && column <= start.column);
        return collinear && betweenRow && betweenColumn;
    }

    public boolean contains(Point p) {
        return contains(p.row, p.column);
    }

    public boolean contains(Line line) {
        return contains(line.end) && contains(line.start);
    }


    public boolean collinear(Line line) {
        int[] myDir = direction();
        int[] lineDir = line.direction();
        boolean thisHaveZero = myDir[0] == 0 || myDir[1] == 0;
        boolean lineHaveZero = lineDir[0] == 0 || lineDir[1] == 0;
        boolean parallelSameAxis = (myDir[0] == 0 && lineDir[0] == 0) || (myDir[1] == 0 && lineDir[1] == 0);
        if (thisHaveZero && lineHaveZero)
            return parallelSameAxis;
        return Math.abs(myDir[0] - myDir[1]) == Math.abs(lineDir[0] - lineDir[1]);
    }

    public boolean collinear(Point p1, Point p2) {
        Line l = new Line(p1, p2);
        return collinear(l);
    }

    public boolean collinear(int startRow, int startColumn, int endRow, int endColumn) {
        return collinear(new Line(startRow, startColumn, endRow, endColumn));
    }

    public int[] direction() {
        return new int[] {Integer.signum(getY()), Integer.signum(getX())};
    }

    @Override
    public String toString() {
        return String.format("Line(%d,%d,%d,%d)", start.row, start.column, end.row, end.column);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Line))
            return false;
        Line l = (Line) obj;
        return (l.end.equals(end) && l.start.equals(start)) || (l.end.equals(start) && l.start.equals(end));
    }

    public static int[] getSign(int row1, int column1, int row2, int column2) {
        return new int[]{Integer.signum(row2 - row1), Integer.signum(column2 - column1)};
    }

    public static int[] getSign(Point p1, Point p2) {
        return Line.getSign(p1.row, p1.column, p2.row, p2.column);
    }

    public boolean isOnSameStraight(Line line, Board b) {
        int[] dir = direction();
        int dim = b.dimension();
        int x = dir[0] * dim;
        int y = dir[1] * dim;
        Line straight = new Line(y, x, -y, -x);
        return straight.contains(this) && straight.contains(line);
    }

    public boolean isOnSameStraight(Point start, Point end, Board b) {
        return isOnSameStraight(new Line(start, end), b);
    }

    public boolean isOnSameStraight(int startRow, int startColumn, int endRow, int endColumn, Board b) {
        return isOnSameStraight(new Line(startRow, startColumn, endRow, endColumn), b);
    }

    public Line getNearestPoints(Line line) {
        LinkedList<Line> lines = new LinkedList<>();
        lines.add(new Line(getStart(), line.getStart()));
        lines.add(new Line(getStart(), line.getEnd()));
        lines.add(new Line(getEnd(), line.getStart()));
        lines.add(new Line(getEnd(), line.getEnd()));
        lines.sort(Comparator.comparingInt(Line::length));
        return lines.getFirst();
    }

    public Point getNearestPointTo(Point p) {
        LinkedList<Line> lines = new LinkedList<>();
        lines.add(new Line(getStart(), p));
        lines.add(new Line(getEnd(), p));
        lines.sort(Comparator.comparingInt(Line::length));
        return lines.getFirst().getStart();
    }

    public Point getNearestPointTo(int row, int column) {
        return getNearestPointTo(new Point(row, column));
    }

}