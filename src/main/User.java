package main;

import java.util.Scanner;

public class User implements Player {

    private Board board;
    private char character;

    public User(char character, Board board) {
        this.character = character;
        this.board = board;
    }

    @Override
    public void turn() {
        board.set(nextStep(), character);
    }

    @Override
    public Point nextStep() {
        System.out.print("Введите координаты следующего хода через пробел [строка, столбец]: ");
        String[] wholeInput = (new Scanner(System.in)).nextLine().trim().split(" ");
        return new Point(Integer.parseInt(wholeInput[0]), Integer.parseInt(wholeInput[1]));
    }

    @Override
    public Board getBoard() {
        return board;
    }

    @Override
    public void setBoard(Board b) {
        board = b;
    }

    @Override
    public char getCharacter() {
        return character;
    }

    @Override
    public void setCharacter(char c) {
        character = c;
    }

}
