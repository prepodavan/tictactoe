package main;

import java.util.Scanner; // 1-2 1-3 2-2 0-2 3-2

public class Game {

    Board board; // " " - empty cell, "o" - cell with zero, "x" - cell with x

    int dimension = -1;

    int lengthOfWinningLine = -1;

    Player firstPlayer = null;
    Player secondPlayer = null;

    Game() {
        System.out.println("Добро пожаловать в крестики нолики!\nДля выхода Ctrl+C");
    }

    void start() {
        while (!initBoard())
            System.out.println("Размерность игрового поля и длина победной линии должны быть не менее 3!");
        firstPlayer = new User(askCharacter(), board);
        secondPlayer = new AI(board, firstPlayer.getCharacter() == 'o' ? 'x' : 'o', lengthOfWinningLine);
        while (!checkWin()) {
            board.show();
            firstPlayer.turn();
            if(checkWin())
                break;
            secondPlayer.turn();
        }
        Player winner =
                board.getCell(board.winningLines.get(0).getStart()) == firstPlayer.getCharacter() ? firstPlayer:secondPlayer;
        board.show();
        System.out.println("Победа " +  winner);
    }

    boolean checkWin() {
        if (board.winningLines.size() > 0)
            return board.winningLines.get(0).length() == lengthOfWinningLine;
        return false;
    }

    char askCharacter() {
        System.out.print("Выберите сторону [o, x]: ");
        String playerChar = (new Scanner(System.in)).nextLine().trim();
        while (!playerChar.equals("o") && !playerChar.equals("x")) {
            System.out.println("Выбран некорректный символ.");
            System.out.print("Выберите сторону [o, x]: ");
            playerChar = (new Scanner(System.in)).nextLine();
        }
        return playerChar.charAt(0);
    }

    boolean initBoard() {
        System.out.print("Через пробел введите размерность игрового поля и длину победной линии: ");
        String[] wholeInput = (new Scanner(System.in)).nextLine().trim().split(" ");
        if (wholeInput.length != 2)
            return false;
        dimension = Integer.parseInt(wholeInput[0]);
        lengthOfWinningLine = Integer.parseInt(wholeInput[1]);
        if (dimension < 3 || lengthOfWinningLine < 3)
            return false;
        board = new Board(dimension, ' ', 'x', 'o');
        return true;
    }

}
