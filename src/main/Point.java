package main;

public class Point {
    int row;
    int column;

    private Point() {
    }

    @Override
    public String toString() {
        return String.format("Point(%d,%d)", row, column);
    }

    Point(int row, int column) {
        this.row = row;
        this.column = column;
    }

    @Override
    public int hashCode() {
        return (int) Math.pow(row, column);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Point ))
            return false;
        return ((Point) obj).column == column && ((Point) obj).row == row;
    }
}